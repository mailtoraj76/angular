import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubjectsComponent } from './subjects/subjects.component';
import { NavbarComponent } from './navbar/navbar.component';
import { StudentsComponent } from './students/students.component';
import { StudentResolverService } from './student-resolver.service';

const routes: Routes = [
  { path: '', component: NavbarComponent },
  { path: 'Subject/:id', component: StudentsComponent, resolve: {subjects: StudentResolverService}},
  { path: 'Subject', component: SubjectsComponent }
  // { path: '**', component: NotFound}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
