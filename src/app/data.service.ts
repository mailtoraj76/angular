import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs';
//import { environment } from './environments/environment';



@Injectable({
  providedIn: 'root'
})
export class DataService {
  readonly SERVER_URL = environment.endPointUrl
  constructor(private http: HttpClient) {}

  getSubjects(): Observable <any> {
    return this.http.get(this.SERVER_URL+'/subjects')
  }
  getUniqueStudents(id: string): Observable <any>{
    return this.http.get(this.SERVER_URL+'/subjects/'+id)
  }
}
