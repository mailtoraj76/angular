import { DataService } from './../data.service';
import { Component, OnInit, APP_ID } from '@angular/core';
import { Subjects } from './subjects'
import { Observable } from 'rxjs'

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.css']
})
export class SubjectsComponent implements OnInit {
  title = 'List of Unique Subjects';
  subs: Subjects[];


  constructor(private dataservice: DataService) { }
  

  ngOnInit() {
    this.dataservice.getSubjects().subscribe (
      data =>
      {
        this.subs = data;
      }
    )
  }

}
