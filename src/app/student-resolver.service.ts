import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { DataService } from './data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
 
@Injectable({
  providedIn: 'root',
})
export class StudentResolverService implements Resolve<any> {
  constructor(private dataservice: DataService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    let id = route.paramMap.get('id');
    console.log('Resolving for subject id:' + id);
    
    return this.dataservice.getUniqueStudents(id)
    .pipe(map( data =>
      {
        if (data){
          console.log(data)
          console.log(state.url)
          return data
          } else {
            console.log ('No data.. redirecting back')
            this.router.navigate(['/Subject'])
          }
      }))
    }
  }
