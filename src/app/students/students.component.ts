import { Component, OnInit } from '@angular/core';
import {Students} from './student'
import { DataService } from './../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  title = 'List of unique students enrolled in selected subject';
  stds: Students[];
  selsub: string;

  constructor(private dataservice: DataService, private route: ActivatedRoute) { }
  
  ngOnInit() {
    this.stds=this.route.snapshot.data['subjects'];
 }

  /* ngOnInit() {
    this.route.paramMap
    .subscribe(params =>
      {
        this.selsub = params.get('id')
      })
    this.dataservice.getUniqueStudents(this.selsub).subscribe (
      data =>
      {
        this.stds = data;
        
      }
    )
  } */

}
